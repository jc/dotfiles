#!/bin/bash





external_monitors=$(xrandr --query | grep 'DP-1-')
if [[ $external_monitors = *connected* ]]; then
    xrandr --output DP-1-3 --mode 1024x768 --rotate normal --right-of eDP-1 --output DP-1-1 --mode 2560x1440 --rotate normal --right-of DP-1-3

fi
