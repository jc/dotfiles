# dotfiles

Welcome to my first ever repository on a website for anyone and everyone to see!

I started out with something easy and very common - so please enjoy my dotfiles.

Most of it (or all of it?) concerns my setup with bspwm as Tiling WM and polybar. While I  adapted a lot of configs I found online on blogs and (mostly) GitHub, I modified a good deal of it (even if it was just to exchange the color codes or icons); I also adapted some shell scripts to my needs/wants. It certainly was a lot of fun, and still is, writing scripts and files and having it result in an awesome window manager!

